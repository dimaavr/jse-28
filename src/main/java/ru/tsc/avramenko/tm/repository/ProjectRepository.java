package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        return list.stream()
                .filter(p -> p.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project findByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable
        final List<Project> projects = findAll(userId);
        return projects.get(index);
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElseThrow(ProcessException::new);
    }

    @NotNull
    @Override
    public Project removeByIndex(@NotNull final String userId, @NotNull final int index) {
        @NotNull
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(this::remove);
        return project.orElseThrow(ProcessException::new);
    }

    @Nullable
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @Nullable
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByIndex(final String userId, @NotNull final int index, @NotNull final Status status) {
        @Nullable
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}